VAP : Versatile Aggregate Profiler
==================================

This is the main VAP repository.
The complete VAP project is composed of two modules: [vap_core](https://bitbucket.org/labjacquespe/vap_core) (developped in C++) and [vap_interface](https://bitbucket.org/labjacquespe/vap_interface) (developped in Java).

The official VAP website is **[here](http://lab-jacques.recherche.usherbrooke.ca/vap/)**

Copyright (C) 2013,2014 Christian Poitras, Charles Coulombe, Pierre-Étienne Jacques  
Université de Sherbrooke. All rights reserved.

Download
--------
The complete VAP binaries can be downloaded [here](https://bitbucket.org/labjacquespe/vap/downloads).
NOTE THAT A NEW VERSION SUPPORTING BIGWIG FORMAT AND CREATING HEATMAP GRAPHS IS COMING SOON!

Requirements
------------
[Java 7 Update 9 or later](http://www.java.com/)

Run
---
Double-click on the jar file, the vap_interface should open. Alternatively, in a terminal or command prompt type : 
```
java -jar vap-1.0.0-all.jar
```

Test
----
1. Click on the "Test VAP" menu of the interface, then "Load test files".
2. Select the output folder in the "Output selection" section.
3. Click on the "Run" button at the bottom left of the interface.
4. The results of the analysis will be generated in the output folder, including a file called "test_graph_all.png" showing the aggregate profiles.

Documentation
-------------
See the [VAP Documentation](http://lab-jacques.recherche.usherbrooke.ca/software_en/vap/documentation/).

Release Notes
-------------
See the [VAP Releases Notes](http://lab-jacques.recherche.usherbrooke.ca/software_en/vap/home/)
to find out about the latest changes.

Bug Reports and Feedback
------------------------
You can report a bug [here](https://bitbucket.org/labjacquespe/vap/issues?status=new&status=open).
You can also directly contact us via email by writing to vap_support at usherbrooke dot ca.

Any and all feedback is welcome.

Licensing
---------
VAP is released under the [GPL license](http://www.gnu.org/licenses/gpl-3.0.txt).
For further details see the COPYING files in respective repositories.
